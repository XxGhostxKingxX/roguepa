﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject
{

    public Text healthText;
    public AudioClip attack;
    public AudioClip collision;
    public AudioClip death;
    public AudioClip eeriemusic;
    public AudioClip encounter;
    public AudioClip encounter2;
    public AudioClip encounter3;
    public AudioClip evillaugh;
    public AudioClip point;
    public AudioClip undertalemusic;
    public AudioClip walking;


    private Animator animator;
    private int playerHealth;
    private int attackPower = 1;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int secondsUntilNextLevel = 1;

    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        playerHealth = GameController.Instance.playerCurrentHealth;
        healthText.text = "HP: " + playerHealth;
    }

    private void OnDisable()
    {
        GameController.Instance.playerCurrentHealth = playerHealth;
    }

    void Update()
    {

        if (!GameController.Instance.isPlayerTurn)
        {
            return;
        }

        CheckIfGameOver();

        int xAxis = 0;
        int yAxis = 0;

        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");

        if (xAxis != 0)
        {
            yAxis = 0;
        }

        if (xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            healthText.text = "HP: " + playerHealth;
            SoundController.Instance.PlaySingle(attack);
            Move<Wall>(xAxis, yAxis);
            SoundController.Instance.PlaySingle(walking);
            GameController.Instance.isPlayerTurn = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Exit")
        {
            Invoke("LoadNewLevel", secondsUntilNextLevel);
            enabled = false;
        }
        else if (objectPlayerCollidedWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(point);
        }
        else if (objectPlayerCollidedWith.tag == "Soda")
        {
            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + " HP\n" + "HP: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(point);
        }
    }

    private void LoadNewLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    protected override void HandleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        wall.DamageWall(attackPower);
    }

    public void TakeDamage(int damageReceived)
    {
        playerHealth -= damageReceived;
        healthText.text = "-" + damageReceived + " HP\n" + "HP: " + playerHealth;
        animator.SetTrigger("playerHurt");
    }

    private void CheckIfGameOver()
    {
        if (playerHealth <= 0)
        {
            GameController.Instance.GameOver();
        }
    }
}